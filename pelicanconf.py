#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Jamie Bliss'
SITENAME = 'astraluma'
SITEURL = ''

BIO = 'To hire, see <a href="https://lumami.biz/">Lumami Software</a>'

PATH = 'content'
THEME = 'theme'

STATIC_PATHS = ['images', 'files']

TIMEZONE = 'America/Detroit'

DEFAULT_LANG = 'en'

PROFILE_IMAGE = ''

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Social widget
# This theme only really allows for five items
SOCIAL = (
	('github', 'https://github.com/AstraLuma/'),
	('gitlab', 'https://gitlab.com/astronouth7303/'),
	('twitter', 'https://twitter.com/AstraLuma'),
	('mastodon', 'https://cybre.space/@astraluma'),
#	('twitch', 'https://twitch.tv/astraluma'),
#	('email', 'jamie.bliss@astro73.com'),
#	('feed', ''),
	('kofi', 'https://ko-fi.com/astraluma'),
)

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
